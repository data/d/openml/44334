# OpenML dataset: Meta_Album_ACT_410_Extended

https://www.openml.org/d/44334

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album MPII Human Pose Dataset Dataset (Extended)**
***
The MPII Human Pose dataset (http://human-pose.mpi-inf.mpg.de/#download) is a state of the art benchmark for evaluation of articulated human pose estimation. It includes around 25 000 images containing over 40 000 people with annotated body joints. The images were systematically collected using an established taxonomy of every day human activities. Overall the dataset covers 410 human activities and each image is provided with an activity label. Each image was extracted from a YouTube video. Like other Meta-Album datasets, this dataset is preprocessed and all images are resized into 128x128 pixels.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/ACT_410.png)

**Meta Album ID**: HUM_ACT.ACT_410  
**Meta Album URL**: [https://meta-album.github.io/datasets/ACT_410.html](https://meta-album.github.io/datasets/ACT_410.html)  
**Domain ID**: HUM_ACT  
**Domain Name**: Human Actions  
**Dataset ID**: ACT_410  
**Dataset Name**: MPII Human Pose Dataset  
**Short Description**: MPII Human Pose Dataset with images of humans performing 410 activities.  
**\# Classes**: 29  
**\# Images**: 2402  
**Keywords**: human actions, sports  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Simplified BSD License  
**License URL(original data release)**: http://human-pose.mpi-inf.mpg.de/#download
http://human-pose.mpi-inf.mpg.de/bsd.txt
 
**License (Meta-Album data release)**: Simplified BSD License  
**License URL (Meta-Album data release)**: [http://human-pose.mpi-inf.mpg.de/bsd.txt](http://human-pose.mpi-inf.mpg.de/bsd.txt)  

**Source**: MPII Human Pose Dataset  
**Source URL**: http://human-pose.mpi-inf.mpg.de/#download  
  
**Original Author**: Mykhaylo Andriluka and Leonid Pishchulin and Peter Gehler and Schiele, Bernt  
**Original contact**: leonid@mpi-inf.mpg.de  

**Meta Album author**: Jilin He  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@INPROCEEDINGS{6909866,  
    author={Andriluka, Mykhaylo and Pishchulin, Leonid and Gehler, Peter and Schiele, Bernt},  booktitle={2014 IEEE Conference on Computer Vision and Pattern Recognition},   
    title={2D Human Pose Estimation: New Benchmark and State of the Art Analysis},   
    year={2014},
    pages={3686-3693},  
    doi={10.1109/CVPR.2014.471}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44271)  [[Mini]](https://www.openml.org/d/44301)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44334) of an [OpenML dataset](https://www.openml.org/d/44334). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44334/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44334/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44334/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

